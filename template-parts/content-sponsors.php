<?php
/**
 * Template part for sponsors
 *
 * @package omed2016
 */

$images = get_field( 'omed_sponsor_images', $post->ID );
if ( empty( $images ) ) {
  return;
}

?>
<div class="container-fluid pageblock wrap">
  <div class="sponsors__block">
    <ul class="sponsors__items">
    <?php foreach( $images as $image ): ?>
      <li class="sponsors__item">
        <img src="<?php echo $image->guid; ?>" alt="<?php echo $image->post_title; ?>" />
      </li>        
    <?php endforeach ?>
    </ul>
    <a href="https://events.jspargo.com/OMED19/Public/enter.aspx" class="btn btn--primary" target="_blank" rel="noopener">Become a sponsor</a>
  </div>
</div>
